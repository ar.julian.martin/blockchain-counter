import Web3 from "web3"
import Auction from "../assets/contracts/Auction.json"

export const getContract = async () => {
    
    if (window.ethereum === undefined) throw "must install a wallet";
    let web3 = new Web3(Web3.givenProvider || "ws://localhost:7545");
    await window.ethereum.request({ method: "eth_requestAccounts" });

    const abi: any = Auction.abi
    const deployedNetwork: any = Auction.networks["5777"];
    const accounts = await web3.eth.getAccounts();

    const contract = new web3.eth.Contract(abi, deployedNetwork && deployedNetwork.address)

    return { accounts, contract }
} 

export const getContractData = async () => {
    const { contract } = await getContract();
    
    const counter = await contract.methods.getCounter().call()
    const adress = await contract.methods.getCurrentAdress().call()

    return { counter, adress }
}

export const updateCounter = async () => {
    const { contract, accounts } = await getContract();
    
    return await contract.methods.updateCounter().send({ from: accounts[0], gas: 80000 });
}

export const closeAuction = async () => {
    const { contract, accounts } = await getContract();
    
    const from = accounts[0];
    const adress = await contract.methods.getCurrentAdress().call()

    if(from !== adress) throw "Only last bid owner can close the auction";

    return await contract.methods.closeAuction().send({ from, gas: 80000 });
}