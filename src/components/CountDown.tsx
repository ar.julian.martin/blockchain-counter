import { useState, useEffect } from "react"

export const useCountDown = (action: () => any, deps: any[]) => {

    const [secs, setSecs] = useState(30)

    useEffect(() => {
        if(secs === 0) return action();

        const to = setTimeout(() => setSecs(s => s - 1), 1000)
        return () => clearTimeout(to)
    }, [secs])

    useEffect(() => {
        setSecs(30)
    }, deps)

    return <>Quedan {secs} segundos</>
}