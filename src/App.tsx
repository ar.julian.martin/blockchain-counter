import './App.css'
import { getContractData, updateCounter, closeAuction } from './utils/contract';
import { useQuery } from "react-query"
import { useCountDown } from './components/CountDown';

function App() {

  const { isLoading, error, data, isFetching } = useQuery('counter', getContractData)

  const CountDown = useCountDown(() => closeAuction(), [data])

  const handleOffer = async () => {
    await updateCounter();
  }

  if (isLoading) return 'Loading...'
  if (error) return 'An error has occurred: ' + error
  if (!data) return 'An error has ocurred: No data'

  const { counter, adress } = data

  return (
    <>
      <h1>Subasta + Blockchain</h1>
      {CountDown}
      <div className="card">
        <button onClick={() => handleOffer()}>
          Offer
        </button>
        <p>
          Actual offer: {counter} ETH.
          <br />
          From Adress: {adress}.
        </p>
      </div>
      <p className="read-the-docs">
        Luego de 30 segundos sin ofertas se cierra la subasta
      </p>
    </>
  )
}

export default App
