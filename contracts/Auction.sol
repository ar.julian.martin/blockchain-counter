// SPDX-License-Identifier: MIT
pragma solidity >=0.4.22 <=0.8.20;

contract Auction {
    uint256 public counter = 0;
    address public current_adress;
    bool public closed = false;

    function getCounter() public view returns (uint256) {
        return counter;
    }

    function getCurrentAdress() public view returns (address) {
        return current_adress;
    }

    function getClosed() public view returns (bool) {
        return closed;
    }

    function updateCounter() external {
        require(closed == false, "Auction closed");
        counter += 1;
        current_adress = msg.sender;
    }

    function closeAuction() external {
        require(current_adress == msg.sender, "Only last bid owner can close the auction");
        closed = true;
    }
}
